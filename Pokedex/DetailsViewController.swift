//
//  DetailsViewController.swift
//  Pokedex
//
//  Created by Shingi Mutandwa on 8/09/2016.
//  Copyright © 2016 Mutandwa. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    var pokemon: Pokemon!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var defenseLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var baseAttackLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var pokedexIdLabel: UILabel!
    @IBOutlet weak var nextEvoLabel: UILabel!
    @IBOutlet weak var currentEvoImage: UIImageView!
    @IBOutlet weak var nextEvoImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = pokemon.name.capitalized
        
        let img = UIImage(named: "\(self.pokemon.pokedexId)")
        mainImage.image = img
        currentEvoImage.image = img
        pokedexIdLabel.text = "\(pokemon.pokedexId)"
        
        pokemon.downloadPokemonDetail {
            
            // this will be when the network call is complete
             self.updateUi()
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }

    func updateUi() {
        
        defenseLabel.text = pokemon.defense
        heightLabel.text = pokemon.height
        weightLabel.text = pokemon.weight
        baseAttackLabel.text = pokemon.attack
        descriptionLabel.text = pokemon.description
        typeLabel.text = pokemon.type
        nextEvoLabel.text = pokemon.nextEvolutionText
        
        if pokemon.nextEvolutionId == "" {
            nextEvoImage.isHidden = true
            nextEvoLabel.text = "No Evolution"
        } else {
            nextEvoImage.image = UIImage(named: pokemon.nextEvolutionId)
            nextEvoImage.isHidden = false
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
