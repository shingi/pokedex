//
//  Constants.swift
//  Pokedex
//
//  Created by Shingi Mutandwa on 8/09/2016.
//  Copyright © 2016 Mutandwa. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon"

// closure
typealias DownloadComplete = () -> ()
