//
//  Pokemon.swift
//  Pokedex
//
//  Created by Shingi Mutandwa on 7/09/2016.
//  Copyright © 2016 Mutandwa. All rights reserved.
//

import Foundation
import Alamofire

class Pokemon {
    private var _name: String!
    private var _pokedexId: Int!
    private var _description: String!
    private var _type: String!
    private var _defense: String!
    private var _height: String!
    private var _weight: String!
    private var _attack: String!
    private var _nextEvolutionText: String!
    private var _pokemonURL: String!
    private var _nextEvolutionId: String!
    
    var name: String {
        return _name;
    }
    
    var pokedexId: Int {
        return _pokedexId
    }
    
    var description: String {
        if _description == nil {
            _description = ""
        }
        return _description
    }
    
    var type: String {
        if _type == nil {
            _type = ""
        }
        return _type
    }
    
    var defense: String {
        if _defense == nil {
            _defense = ""
        }
        return _defense
    }
    
    var height: String {
        if _height == nil {
            _height = ""
        }
        return _height
    }
    
    var weight: String {
        if _weight == nil {
            _weight = ""
        }
        return _weight
    }
    
    var attack: String {
        if _attack == nil {
            _attack = ""
        }
        return _attack
    }
    
    var nextEvolutionText: String {
        if _nextEvolutionText == nil {
            _nextEvolutionText = ""
        }
        return _nextEvolutionText
    }
    
    var nextEvolutionId: String {
        if _nextEvolutionId == nil {
            _nextEvolutionId = ""
        }
        return _nextEvolutionId
    }
    
    init(name: String, pokedexId: Int) {
        _name = name
        _pokedexId = pokedexId
        
        // set url for this pokemon
        _pokemonURL = "\(URL_BASE)\(URL_POKEMON)/\(self.pokedexId)/"
        
    }
    
    func downloadPokemonDetail(completed: @escaping DownloadComplete) {
        
        Alamofire.request(_pokemonURL).responseJSON { (response) in
            
            if let respDict = response.result.value as? Dictionary<String, AnyObject> {
                
                if let weight = respDict["weight"] as? String {
                    self._weight = weight
                }
                
                if let height = respDict["height"] as? String {
                    self._height = height
                }
                
                if let attack = respDict["attack"] as? Int {
                    self._attack = "\(attack)"
                }
                
                if let defense = respDict["defense"] as? Int {
                    self._defense = "\(defense)"
                }
                
                if let descriptions = respDict["descriptions"] as? [Dictionary<String, AnyObject>] , descriptions.count > 0 {
                    
                    if  let firstDescriptionUrl  = descriptions[0]["resource_uri"] {
                        
                        Alamofire.request("\(URL_BASE)\(firstDescriptionUrl)").responseJSON(completionHandler: { (resp) in
                            
                            if let descriptionResponseDict = resp.result.value as? Dictionary<String, AnyObject> {
                                
                                if let description = descriptionResponseDict["description"] as? String {
                                    self._description = description
                                }
                            }
                            
                            completed()
                        })
                    }
                }
                
                if let types = respDict["types"] as? [Dictionary<String, AnyObject>] , types.count > 0 {
                    
                    if let name = types[0]["name"] as? String {
                        self._type = name.capitalized
                    }
                    
                    // if types are more than one,
                    if types.count > 1 {
                        for x in 1..<types.count {
                            if let name = types[x]["name"] as? String {
                                self._type! += "/\(name.capitalized)"
                            }
                        }
                    }
                }
                
                if let evolutions = respDict["evolutions"] as? [Dictionary<String, AnyObject>], evolutions.count > 0 {
                    
                    let evolution = evolutions[0]
                    
                    if let to = evolution["to"] as? String {
                        
                        self._nextEvolutionText = "NEXT EVOLUTION: \(to)"
                    }
                    
                    if let level = evolution["level"] as? Int {
                        
                        self._nextEvolutionText! += " LVL \(level)"
                    }
                    
                    if let resource_uri = evolution["resource_uri"] as? String {
                        self._nextEvolutionId = resource_uri.replacingOccurrences(of: "/api/v1/pokemon/", with: "").replacingOccurrences(of: "/", with: "")
                    }
                    
                } else {
                    self._nextEvolutionText = "No Evolution"
                }
            }
            
            completed()
        }
    }
}
