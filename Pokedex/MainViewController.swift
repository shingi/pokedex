//
//  MainViewController.swift
//  Pokedex
//
//  Created by Shingi Mutandwa on 7/09/2016.
//  Copyright © 2016 Mutandwa. All rights reserved.
//

import UIKit
import AVFoundation

class MainViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var pokemons = [Pokemon]()
    var musicPlayer: AVAudioPlayer!
    var isInSearchMode = false
    var filteredPokemons = [Pokemon]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        parsePokemonCsv()
        
        initAudio()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func musicButtonPressed(_ sender: UIButton) {
        
        // handle pausing and un-pausing
        if musicPlayer.isPlaying {
            musicPlayer.pause()
            sender.alpha = 0.2
        } else {
            musicPlayer.play()
            sender.alpha = 1.0
        }
    }
    
    func parsePokemonCsv() {
        
        let path = Bundle.main.path(forResource: "pokemon", ofType: "csv")
        do {
            
            let csv = try CSV(contentsOfURL: path!)
            let rows = csv.rows
            print(rows.count)
            
            for row in rows {
                let pokeId = Int(row["id"]!)!
                let name = row["identifier"]!
                let pokemon = Pokemon(name: name, pokedexId: pokeId)
                
                self.pokemons.append(pokemon)
            }
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func initAudio() {
        
        let path = Bundle.main.path(forResource: "music", ofType: "mp3")
        do {
            musicPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path!))
            musicPlayer.prepareToPlay()
            musicPlayer.numberOfLoops = 1
            musicPlayer.play()
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            if identifier  == "ShowDetails" {
                
                if let destination = segue.destination as? DetailsViewController {
                    
                    destination.pokemon = sender as! Pokemon
                }
            }
        }
    }

}


extension MainViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 105, height: 105)
    }
}

extension MainViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isInSearchMode {
            return self.filteredPokemons.count
        }
        
        return self.pokemons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PokeCell", for: indexPath) as? PokeCell {
        
            let pokemon: Pokemon!
            
            if isInSearchMode {
                pokemon = self.filteredPokemons[indexPath.row]
            } else {
                pokemon = self.pokemons[indexPath.row]
            }
        
            cell.configureCell(pokemon)
            
            return cell
        }
        
        return UICollectionViewCell()
    }
}

extension MainViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let pokemon: Pokemon!
        if isInSearchMode {
            pokemon = self.filteredPokemons[indexPath.row]
        } else {
            pokemon = self.pokemons[indexPath.row]
        }
        performSegue(withIdentifier: "ShowDetails", sender: pokemon)
    }
}

extension MainViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == nil || searchBar.text == "" {
            self.isInSearchMode = false
            filteredPokemons = [Pokemon]()
            self.collectionView.reloadData()
            self.view.endEditing(true)
        } else {
            self.isInSearchMode = true
            let lowercasedText =  (searchBar.text?.lowercased())!
            filteredPokemons = self.pokemons.filter({ $0.name.range(of: lowercasedText) != nil })
            self.collectionView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.view.endEditing(true)
    }
}

